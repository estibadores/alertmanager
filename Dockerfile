FROM registry.sindominio.net/debian

RUN apt-get update && \
		apt-get upgrade && \
    apt-get -qy install --no-install-recommends prometheus-alertmanager ca-certificates &&\
    apt-get clean

VOLUME ["/etc/prometheus","/var/lib/prometheus"]

CMD /usr/bin/prometheus-alertmanager --log.level=debug
